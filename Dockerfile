ARG NODE_VERSION
FROM node:${NODE_VERSION}-slim
ENV PHANTOMJS_CDNURL https://gitlab.com/iwbc/docker-website-coding-kit/-/raw/master
RUN apt-get update && apt-get -y install bzip2 openssh-client rsync git libpng-dev