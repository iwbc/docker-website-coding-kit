#!/bin/bash

versions=(4 6 8 10 12 14)

for version in ${versions[@]}; do
  docker build --platform linux/amd64 --build-arg NODE_VERSION=$version -t registry.gitlab.com/iwbc/docker-website-coding-kit:node-$version .
  if [ "$1" = "push" ]; then
    docker push registry.gitlab.com/iwbc/docker-website-coding-kit:node-$version
  fi
done
